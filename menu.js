var { remote } = require('electron')

const TitleBar = function() {
    return '                                                        \
        <div id="titlebar">                                         \
        <div id="drag-region">                                      \
            <div id="window-title"><span>Batotwo</span></div>       \
            <div id="window-controls">                              \
                <div class="button" id="min-button">                \
                    <span>&#xE921;</span>                           \
                </div>                                              \
                <div class="button" id="max-button">                \
                    <span>&#xE922;</span>                           \
                </div>                                              \
                <div class="button" id="restore-button">            \
                    <span>&#xE923;</span>                           \
                </div>                                              \
                <div class="button" id="close-button">              \
                    <span>&#xE8BB;</span>                           \
                </div>                                              \
            </div>                                                  \
        </div>                                                      \
    </div>                                                          \
    ';
}

function init() {
    let window = remote.getCurrentWindow();
    const minButton = document.getElementById('min-button'),
        maxButton = document.getElementById('max-button'),
        restoreButton = document.getElementById('restore-button'),
        closeButton = document.getElementById('close-button');

    minButton.addEventListener("click", event => {
        window = remote.getCurrentWindow();
        window.minimize();
    });

    maxButton.addEventListener("click", event => {
        window = remote.getCurrentWindow();
        window.maximize();
        toggleMaxRestoreButtons();
    });

    restoreButton.addEventListener("click", event => {
        window = remote.getCurrentWindow();
        window.unmaximize();
        toggleMaxRestoreButtons();
    });

    // Toggle maximise/restore buttons when maximisation/unmaximisation
    // occurs by means other than button clicks e.g. double-clicking
    // the title bar:
    toggleMaxRestoreButtons();
    window.on('maximize', toggleMaxRestoreButtons);
    window.on('unmaximize', toggleMaxRestoreButtons);

    closeButton.addEventListener("click", event => {
        window = remote.getCurrentWindow();
        window.close();
    });

    function toggleMaxRestoreButtons() {
        window = remote.getCurrentWindow();
        if (window.isMaximized()) {
            maxButton.style.display = "none";
            restoreButton.style.display = "flex";
        } else {
            restoreButton.style.display = "none";
            maxButton.style.display = "flex";
        }
    }
}  

exports.TitleBar = TitleBar;
exports.init = init;
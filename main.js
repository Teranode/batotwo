// Modules to control application life and create native browser window
const {app, shell, BrowserWindow, ipcMain } = require('electron'),
      child = require('child_process').execFile,
      url = require('url'),
      path = require('path')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Main Browser Window
    mainWindow = new BrowserWindow({
        width: 1280,
        height: 720,
        minWidth: 640,
        minHeight: 360,
        frame: false,
        show: false,
        resizable: true,
        title: "Batotwo",
        backgroundColor: '#14181f',
        webPreferences: {
            nodeIntegration: false,
            preload: path.join(__dirname, 'webview-preload.js')         
        }
    })

    mainWindow.loadURL(url.format({
        pathname: 'desktop.batotwo.me',
        protocol: 'https:',
        slashes: true
    })) 

    mainWindow.once('ready-to-show', () => {
        mainWindow.maximize()
        mainWindow.show()
    })

    ipcMain.on('application-launch', (event, data) => {
        var executablePath = data.application;
        var parameters = data.parameters;

        if(!Array.isArray(parameters) || !parameters.length)
            child(executablePath)
        else 
            child(executablePath, parameters)
    })

    //mainWindow.webContents.openDevTools()

    app.on('web-contents-created', (e, contents) => {
        contents.on('will-attach-webview', (event, webPreferences, params) => {
            // Strip away preload scripts if unused or verify their location is legitimate
            delete webPreferences.preload
            delete webPreferences.preloadURL

            // Disable Node.js integration
            webPreferences.nodeIntegration = false

            // Verify URL being loaded
            if (!params.src.startsWith('https://desktop.batotwo.me')) {
            event.preventDefault()
            }
        })

        // Check for a webview
        if (contents.getType() == 'webview') {
            // Listen for any new window events
            contents.on('new-window', (e, url) => {
                e.preventDefault()
                shell.openExternal(url)
            })
        }
    })

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})
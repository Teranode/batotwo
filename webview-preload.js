const {
    remote,
    ipcRenderer
} = require('electron');
const menu = require('./menu.js');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const findFile = require('then-recursively-search');

let mainData, game_file, location, user_location, parameters, applicationButton

document.addEventListener("DOMContentLoaded", e => {

    document.body.insertAdjacentHTML('afterbegin', menu.TitleBar())
    document.querySelector('#layout-content').classList.add('titlebar-present');
    menu.init();

    $(document).on('ajaxUpdate', ev => {
        applicationButton = $('#application-button');

        if (applicationButton.length) {

            applicationButton.parent().removeAttr('uk-tooltip')

            fetch(applicationButton.data('url'))
                .then((resp) => resp.json())
                .then((data) => {

                    mainData = data.data
                    parameters = mainData.application_parameters

                    if (process.platform !== 'darwin') {
                        game_file = mainData.application_windows_file
                        user_location = mainData.application_location
                        location = mainData.application_windows_location

                        checkGame(location, user_location ? false : true);
                    }
                })
                .catch(error => console.log(error));
        }
    })
});

document.addEventListener("click", e => {

    if (e.target.id == 'application-button') {
        var applicationData = {
            "parameters": parameters,
            "application": applicationButton.data("app"),
        };

        ipcRenderer.send("application-launch", applicationData);
    }

    if (e.target.id == 'gameFolder') {
        remote.dialog.showOpenDialog({
                title: 'Select Game Folder',
                properties: ['openDirectory']
            },
            folder => {
                if (folder === undefined) {
                    e.target.value = null;
                } else {
                    e.target.value = folder[0];
                }
            }
        );
    }
});

async function checkGame(game_loc, query) {
    if (query) {
        const {
            stdout,
            stderr
        } = await exec(`\
        Powershell /command "Get-ChildItem -Path HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall, \
        HKLM:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall | Get-ItemProperty | \
        Where-Object {$_.DisplayName -match '${game_loc}'} | Select-Object -exp InstallLocation | Format-List"`);

        output = stdout.trim().split("\n")

        var appLocation = output[0].substr(0, output[0].lastIndexOf('\\'))
    }

    var loc = appLocation ? appLocation : user_location

    findFile(loc, game_file)
        .then(result => {
            applicationButton.removeAttr('disabled')
            applicationButton.data('app', result)
        })
        .catch(error => {
            UIkit.tooltip(applicationButton.parent(), {
                title: `Game Not Installed`
            })
        });
}